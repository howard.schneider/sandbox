# sandbox

This repository can be used to test out different pieces of code, both internal as well as external.
Please push any code to this repository.


Note: Project CCA2 is created as a private repository
All source code from GitHub public repository (only type allowed in the free account) will be
transferred to the GitLab private repository, and made public eventually with completion of CCA2.
Contact me please for access to CCA2.

-Causal Cognitive Architecture 2  code re-write.
-Includes meaningfulness in ANN/Hopfield nets, more robust navigational module, more robust instinctive and learned primitives
-Augmented mode includes multiple navigational modules
